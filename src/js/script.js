alert('Theory in Readme file');

const userAge = prompt("Введіть свій вік:");

if (!isNaN(userAge) && userAge != ' '){
  if(userAge < 12){
    alert('Ви дитина!');
  } else if(userAge >= 12 && userAge < 18){
    alert('Ви підліток!');
  } else {
    alert('Ви дорослий(-а)!');
  }
} else {
  alert('Ви ввели не число!');
}

const userMonth = prompt('Введіть місяць:').toLocaleLowerCase();

switch(userMonth){
  case 'січень': {
    console.log('У січні 31 день.');
    break;
  }
  case 'лютий': {
    console.log('У лютому 28 днів.');
    break;
  }
  case 'березень': {
    console.log('У березні 31 день.');
    break;
  }
  case 'квітень': {
    console.log('У квітні 30 днів.');
    break;
  }
  case 'травень': {
    console.log('У травні 31 день.');
    break;
  }
  case 'червень': {
    console.log('У червні 30 днів.');
    break;
  }
  case 'липень': {
    console.log('У липні 31 день.');
    break;
  }
  case 'серпень': {
    console.log('У серпні 31 день.');
    break;
  }
  case 'вересень': {
    console.log('У вересні 30 днів.');
    break;
  }
  case 'жовтень': {
    console.log('У жовтні 31 день.');
    break;
  }
  case 'листопад': {
    console.log('У листопаді 30 днів.');
    break;
  }
  case 'грудень': {
    console.log('У грудні 31 день.');
    break;
  }
  default: {
    console.log('Місяць введено не правильно.')
    break;
  }
}
